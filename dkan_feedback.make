api: 2
core: 7.x
defaults:
  projects:
    subdir: contrib
projects:
  rate:
    version: '1.7'
  votingapi:
    version: '2.12'
  captcha:
    version: '1.5'
